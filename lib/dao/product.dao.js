import { readFileSync } from "fs";
import { join } from "path";
const productsFilePath = join(process.cwd(), 'data', 'products.json');
let productDataCache = null;


export function getProductField(fieldName) {
  const products = this.getAllProducts();
  return products.reduce((acc, product) => {
    const productField = {};
    productField[fieldName] = product[fieldName];
    acc.push(productField)
    return acc;
  }, [])
}
export function getAllProducts() {
  return loadProductData();
}
function loadProductData() {
  if (!productDataCache) {
    const fileContents = readFileSync(productsFilePath, 'utf-8');
    productDataCache = JSON.parse(fileContents);
  }
  return productDataCache.products.product;
}
