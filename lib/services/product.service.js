import { getAllProducts } from '../dao/product.dao.js';



export function getProducts() {
  return getAllProducts();
}

export function getProduct(productSku) {
  return getProducts().filter(product => product.sku === productSku)[0];
}

export function getAllProductIds() {
  const products = getAllProducts();
  return products.map(product => {
    return {
      params: {
        sku: product.sku
      }
    }
  })
}
