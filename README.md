# Claimazon


## Application design

Claimazon application is built with [Next.js](https://nextjs.org/) framework. 

The landing page is client side rendered as in the product content can change, i.e. paginated, certain catalogs & categories shown etc.

The product details page is SSR as the product details design would look the same.

## Execution

To run the application 

- To run the application, checkout the code from the repo, or unzip the zip file to a location.
- Install [node.js](https://nodejs.org/en/)
- Navigate to the path the code is saved
- Run `yarn install` to setup the dependencies
- A shell script is provided to make invoking the nodejs application easy, the script is in the `bin` folder and can be invoked as `sh run-claimazon.sh`, run this script and navigate to url `http://localhost:3000`
- To run tests run the provided scripts i.e. `sh ./bin/run-unit-tests.sh`
