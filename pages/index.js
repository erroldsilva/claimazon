import Head from 'next/head'


import Layout, { siteTitle } from '../components/layout'
import AllProductsView from '../components/allProducts';

export default function Home() {
  return (
    <Layout home>
      <div>
        <Head>
          <title>{siteTitle}</title>
        </Head>
        <section>
          <header>
            <h2>
              Catalog
            </h2>
          </header>
          <AllProductsView></AllProductsView>
        </section>
      </div>
    </Layout>
  )
}
