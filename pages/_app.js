import '../styles/mvp.css';
import '../styles/overwrite.css';
export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
