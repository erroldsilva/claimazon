import { getProducts } from "../../../lib/services/product.service"




export default function handler(req, res) {
  if (req.method === 'GET') {
    const products = getProducts();
    res.json(products);
 }
}
