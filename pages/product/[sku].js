import { getAllProductIds, getProduct } from '../../lib/services/product.service';

import ProductView from '../../components/productDetails';


export default function product({ productDetails }) {
  return (
    <ProductView productDetails={productDetails}></ProductView>
  )
}

export async function getStaticPaths() {
  const paths = getAllProductIds();
  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const { sku } = params;
  const productDetails = await getProduct(sku)
  return {
    props: {
      productDetails
    }
  }
}
