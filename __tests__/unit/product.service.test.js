
import * as productDaoMock from '../../lib/dao/product.dao.js';
import * as productService from '../../lib/services/product.service';

jest.mock('../../lib/dao/product.dao.js');
describe('Products Service', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  test('all products are retrieved', () => {
    const mockProducts = [{ sku: 1 }];
    productDaoMock.getAllProducts.mockReturnValue(mockProducts);
    const result = productService.getProducts()
    expect(result).toEqual(mockProducts);
  });

  test('a specific product is retrieved', () => {
    const mockProducts = [{ sku: 1 }, { sku: 2 }];
    productDaoMock.getAllProducts.mockReturnValue(mockProducts);
    const result = productService.getProduct(1);
    expect(result).toEqual(mockProducts[0]);
  });

  test('all product ids are retrieved', () => {
    const mockProducts = [{ sku: 1 }, { sku: 2 }];
    productDaoMock.getAllProducts.mockReturnValue(mockProducts);
    const result = productService.getAllProductIds();
    expect(result).toEqual([{ "params": { "sku": 1 } }, { "params": { "sku": 2 } }]);
  })
})
