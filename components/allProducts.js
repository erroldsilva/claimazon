import Link from 'next/link'
import useSWR from 'swr'


const fetcher = (...args) => fetch(...args).then((res) => res.json());
export default function AllProductsView() {
  const { data: products = [] } = useSWR('/api/products', fetcher);
  return (
    products.map(product => {
      return (
        <aside key={product.sku}>
          <figure>
            <img alt="Product Image" src={product.thumbnail} />
          </figure>
          <Link href={`/product/${product.sku}`} >
            <a>
              <p>{product.title}</p>
            </a>
          </Link>
          <p>
            {product.author}
          </p>
          <p>
            <small>
              Price: {product.price} {product.currency}
            </small>
          </p>
        </aside>
      )
    })
  )
}
