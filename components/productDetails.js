
import Layout from './layout';
import styles from './productDetails.module.css'
export default function ProductView({ productDetails }) {
  return (
    <Layout>
      <section className={styles.productDetailsSection}>
        <div className={styles.productDetails}>
          <p>
            <h3>
              {productDetails.title}
            </h3>
          </p>
          <figure>
            <img alt="Product Image" src={productDetails.thumbnail} />
          </figure>
          <p>
            {productDetails.author}
          </p>
          <p>
            {productDetails.description}
          </p>
          <p>
            <small>
              Price: {productDetails.price} {productDetails.currency}
            </small>
          </p>
          <div>
            {productDetails.tags.map(tag => {
              return (
                <span key={tag.id} className={styles.productTag}>{tag.name }</span>
              )
            })}
          </div>
        </div>
        <div >
          {
            <aside hidden={!Boolean(productDetails.reviews)}>
              {productDetails.reviews && productDetails.reviews.map((review) => {
                return (
                  <p key={review.id} className={styles.reviewPara}>
                    <span>
                      Rating: {review.rating}
                    </span>
                    <div hidden={!Boolean(review.message)}>
                      {review.message}
                    </div>
                  </p>
                )
              })}
            </aside>
          }
        </div>
      </section>
    </Layout>
  )
}
