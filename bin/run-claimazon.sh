#!/bin/sh

BASEDIR=$(dirname $0)
cd "$BASEDIR/../" || return
yarn build
yarn start
